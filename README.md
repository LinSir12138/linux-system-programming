# Linux系统编程

### 介绍
陈悦老师的“Linux系统编程”课对应的仓库，存放一些上课的代码以及资料等。

##### （1）文件存放位置

> code

使用阿里云的轻量级应用服务器，文件统一放在 `/home/linkai` 文件夹内，每个礼拜创建一个文件夹，命名为 week1,2.. 主要存放上课的代码

> document

笔记记在本地电脑上面，课后将服务器上的代码拷贝的本地，然后push到码云仓库。

本地主要以 Markdown格式记录笔记，同级的 image 文件夹存放笔记用到的图片。

### 其他

- 发现了一个学习 Linux 命令github项目，仓库地址：https://github.com/jaywcjlove/linux-command

  - 预览地址：https://wangchujiang.com/linux-command/

### 第1周_2020年9月21日

> 主要内容

- Linux 文件目录，gcc 相关操作，vim 相关命令

### 第2周_2020年9月28日

- Linux 文件属性；脚本命令；strace命令跟踪执行。

