# 一、安装优麒麟18.04

##### （1）去官网下载18.04版本的 iso 镜像文件

文件2个G左右

（2）用VMWare安装

安装差不多半小时左右吧。登录系统之后，会让你选择更新到20.04版本，不要更新。

### 1.5.2 更换心脏

#### （2）实验步骤

> sudo apt-get install libncurses5-dev libssl-dev build-essential openssl

输入命令之后，会让你输入登录密码，然后让你选择是否继续，按下 y 即可。

安装成功之后
![image-20201009163319160](images\image-20201009163319160.png)

> 解压

解压也要一定的时间，耐心等待

> 复制自带的配置文件

注意：不能全部粘贴代码，文件名要和自己的对应起来，可以输入  `ll /boot` 来查看一下配置文件的名称。

> 查看核心数

我这里显示 processor 数是 1，emm~好奇怪啊，就离谱

遇到问题：

![image-20201009170540573](images\image-20201009170540573.png)

解决：

参考csdn：https://blog.csdn.net/weixin_43568960/article/details/104565781

- 进入解压后的目录，输入 `make menuconfig`

![image-20201009171048730](images\image-20201009171048730.png)

- 这里我选择复制优麒麟Linux 系统中自带的配置文件

终于编译完了

![image-20201009214012844](images\image-20201009214012844.png)