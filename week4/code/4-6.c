#include "ch03.h"
int main()
{
	int fd;
	if((fd = open("./out.dat", O_WRONLY|O_CREAT, 0644)) == -1)
	{
		err_exit("open!");
	}
	if(dup2(fd, STDOUT_FILENO) == -1)
	{
		err_exit("Redirect standard output failed");
	}
	printf("this is a test program for redirect \n");
	close(fd);
	return 0;
}
