#include "ch03.h"
char buf1[] = "abcdefghij";
char buf2[] = "ABCDEFGHIJ";
int main(void)
{
	int fd;
	fd = open("file.hole", O_WRONLY|O_CREAT|O_APPEND, 0644);
	// 将buf1中的字符写入文件中
	write(fd, buf1, 10);
	// 写完之后，当前文件位置为10，下面将它移到40
	lseek(fd, 40, SEEK_SET);
	// 完成操作后，现在文件位置为40,然后在write写入10个位置
	write(fd, buf2, 10);
	// 最终文件位置为50
	return 0;
}
