#include "ch03.h"
int main(void)
{
	int fd;
	fd = open("myoutput", O_WRONLY|O_CREAT, 0644);
	// 下面2条语句的效果是一样的
	dup2(fd, 1);
	//dup2(fd, STDOUT_FILENO);
	printf("完成了输出重定向，将输出到文件描述字fd所打开的文件中！\n");
	close(fd);
	return 0;
}
