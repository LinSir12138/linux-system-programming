进程的运行 = 计算任务 + IO

​						指令			资源

​						CPU			外设

​						ns				minute

端口 --》 固定的内存映射地址

-----

进程通信

- os内核转发         ns
- 共享内存           ns
- 共享文件        s（太慢了）

---

对文件操作

- 基于文件描述字
  - create
  - open
- 基于流（流建立在文件描述字之上）
  - fopen
  - fread
  - fwrite
  - fclose

底层都是基于 ”文件描述字“ 的，在上面封装一层形成流

---

0  八进制

644 权限

open 加第4个参数也不会报错

open 可以新建，和打开文件

c语言可以通过可变参数ls

> 3-1-1.c

```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(void)
{
	int fd = open("./test.txt", O_CREAT, 0644, "test");
	printf("file descriptor = %d \n", fd);
	close(fd);
	return 0;
}

```

`gcc -g -Wall 3-1-1.c -o 3-1-1`

> 3-1-2.c

```c

```

gcc -E    3-1-2.c  -o  3-1-2.i

---

open函数，第3个参数是 ... 说明是可变参数，以这种方式支持了 “重载”

---

gcc 要复习

---

p31 页，尝试使用 fopen 来新建

---

`man  fwrite`    用来查看某个函数帮助文档

gcc -g -Wall xx.c -o xxx

> 下午 加上 fread 功能

数据写入文件，然后在使用  fread 读取出来

> 3-1-3.c

从键盘输入字符，显示到屏幕上，同时写入到文件中

```c
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

int main(void)
{
	FILE * fp = fopen("./ftest.txt", "w");
	char buf[80];
	int ret = 0;
	memset(buf, 0, sizeof(buf));
	fgets(buf, sizeof(buf), stdin);
//	printf("file descriptor = %d", fp);
	ret =  fwrite(buf, sizeof(buf), 1, fp);
	printf("ret = %d", ret);
	fclose(fp);
	return 0;
}
```

> 

gcc -E test1.c -o test1.i

----

文件操作：

- 由 os 提供的基于文件描述字，更为底层

- 用标准函数库提供的，基于 FILE流，这个API， FILE（句柄）是对包括文件描述字在内的底层文件信息的封装。
  - 关于 file 定义，在 libio.h 

---

> 实验测试 fopen 和 fdopen 的区别

