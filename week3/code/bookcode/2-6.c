#include "ch02.h"
int main()
{
	// 测试文件定位
	FILE * stream;
	long int x;
	stream = fopen("test_file", "r");
	x = ftell(stream);
	printf("first: x = %d\n", x);
	fseek(stream, 5, SEEK_SET);
	x = ftell(stream);
	printf("second: x = %d\n", x);
	rewind(stream);
	x = ftell(stream);
	printf("end: x = %d\n", x);
	return 0;
}
