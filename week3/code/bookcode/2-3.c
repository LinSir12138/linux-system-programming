#include "ch02.h"
int main()
{
	FILE * stream;
	char buf[80];
	// getline 读入的字符数
	int numbers;
	int n = 6;
	stream = fopen("test_file", "r");
//	从 test_file 中读取6个字符（包括换行符）
//	fgets(buf, 6, stream);
//	printf("the text is %s \n", buf);


	// 测试 getline,有 bug
	numbers = getline(&buf, &n, stream);
	printf("the text is %s ", buf);
	fclose(stream);
	return 0;
	
}
