#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(void)
{
	// 输入一个字符串，同时输出在屏幕上面
	FILE *fp = fopen("./ftest.txt", "w");
	char buf[80];
	//memset(buf, 0, sizeof(buf));
	fgets(buf, sizeof(buf), stdin);
	printf("the string is \"%s\"\n", buf);
	// 把buffer里的数据写入到文件
	//fwrite(buf, sizeof(buf),1, fp);
	fclose(fp);
	return 0;
}
