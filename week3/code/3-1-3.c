#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

int main(void)
{
	FILE * fp = fopen("./ftest.txt", "w");
	char buf[80];
	int ret = 0;
	memset(buf, 0, sizeof(buf));
	fgets(buf, sizeof(buf), stdin);
//	printf("file descriptor = %d", fp);
//	一次性全部把文件写进去
	ret =  fwrite(buf, sizeof(buf), 1, fp);
	
// 	分4次吧文件写入
	//ret = fwrite(buf, sizeof(buf), 4, fp);
	
	//ret = fwrite(buf, sizeof(buf)/2, 1, fp);

	printf("ret = %d", ret);
	fclose(fp);
	return 0;
}
