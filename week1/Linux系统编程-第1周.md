> 说明

主要内容：linux目录结构，gcc的操作，vim相关命令

由于之前对 linux 比较熟悉，所以笔记不多。

### Linux 目录结构

- /bin： bin是Binary的缩写, 这个目录存放着最经常使用的命令。
- /boot： 这里存放的是启动Linux时使用的一些核心文件，包括一些连接文件以及镜像文件。（不
  要动）
- /dev ： dev是Device(设备)的缩写, 存放的是Linux的外部设备，在Linux中访问设备的方式和访问文件的方式是相同的。
- **/etc： 这个目录用来存放所有的系统管理所需要的配置文件和子目录。**
- **/home：用户的主目录，在Linux中，每个用户都有一个自己的目录，一般该目录名是以用户的账号命名的。**
- /lib： 这个目录里存放着系统最基本的动态连接共享库，其作用类似于Windows里的DLL文件。
  （不要动）
- /lost+found： 这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件。（存放突然关机的一些文件）
- /media：linux系统会自动识别一些设备，例如U盘、光驱等等，当识别后，linux会把识别的设备挂载到这个目录下。
- /mnt：系统提供该目录是为了让用户临时挂载别的文件系统的，我们可以将光驱挂载在/mnt/上，然后进入该目录就可以查看光驱里的内容了。（我们后面会把一些本地文件挂载在这个目录下）
- **/opt：这是给主机额外安装软件所摆放的目录。比如你安装一个ORACLE数据库则就可以放到这个目录下。默认是空的。**
- /proc： 这个目录是一个虚拟的目录，它是系统内存的映射，我们可以通过直接访问这个目录来获取系统信息。（不用管）
- **/root：该目录为系统管理员，也称作超级权限者的用户主目录。开机刚进入的目录就是这个目录**
- /sbin：s就是Super User的意思，这里存放的是系统管理员使用的系统管理程序。
- /srv：该目录存放一些服务启动之后需要提取的数据。
- /sys：这是linux2.6内核的一个很大的变化。该目录下安装了2.6内核中新出现的一个文件系统
  sysfs 。
- **/tmp：这个目录是用来存放一些临时文件的。用完即丢的文件，可以放在这个目录下，安装包！**
- **/usr：这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于windows下的program files目录。**
- /usr/bin： 系统用户使用的应用程序。
- /usr/sbin： 超级用户使用的比较高级的管理程序和系统守护程序。Super
- /usr/src： 内核源代码默认的放置目录。
  /var：这个目录中存放着在不断扩充着的东西，我们习惯将那些经常被修改的目录放在这个目录下。包括各种日志文件。
- **/run：是一个临时文件系统，存储系统启动以来的信息。当系统重启时，这个目录下的文件应该被删掉或清除。**
- **/www：存放服务器网站相关的资源，环境，网站的项目**

#### 二进制文件解析

objdump -d 1-1 > out.dat		输出到文件

objdump 反汇编，查看可执行文件是二进制码

readclf		查看elf 文件

objdump	对二进制文件进行反汇编

------

### gcc 命令详解

> 参考资料

- https://www.runoob.com/w3cnote/gcc-parameter-detail.html
- https://www.linuxprobe.com/command-gcc.html

C语言源文件变成可执行文件的过程：

![image-20200921164126556](images\image-20200921164126556.png)

假设我们当前目录存在 `test.c`  和 `test2.c` 文件，文件内容为

```c
#include <stdio.h>
int main()
{
	printf("Hello World!\n");
	return 0;
}
```

下面讲解各种 gcc 命令

```bash
gcc test.c
# 将test.c预处理、汇编、编译并链接形成可执行文件。这里未指定输出文件，默认输出为a.out

gcc test.c -o test
# 将test.c预处理、汇编、编译并链接形成可执行文件test。
# -o 选项用来指定输出文件的文件名

gcc -g test.c -o test
# 对应上面的命令，同时生成调试信息，预处理、汇编、编译并链接形成可执行文件，
没加 -g 就没有生成符号表，gdb 就无法进行调试

------------------------------------------------------------------------

gcc -E test.c -o test.i
# 将 test.c  预处理生成 test.i 文件

gcc -E test.c > test.i
# 将 test.c  预处理生成 test.i 文件; 使用 >  输出转向

> 只能输出文本文件
-----------------------------------------------------------------------


gcc -c test.c			# 将 test.c 编译成为 test.o
gcc -c test2.c
gcc -o test.o test2.o -o test		# 将 test.o 和 test2.o 链接成 test

--------------------------------------------------------------------

gcc -S test.c -o test.s
# 只激活预处理和编译，就是指把文件编译成为汇编代码;



gdb  调试可执行文件		太简单，没必要调试

(gdb) run
# 运行

# 加断点
(dgb) break  5

(gdb) quit	# 退出
```

- 查看帮助文档的信息

```bash
[root@jacklin operating-system]# gcc --help
Usage: gcc [options] file...
Options:
  -pass-exit-codes         Exit with highest error code from a phase
  --help                   Display this information
  --target-help            Display target specific command line options
  --help={common|optimizers|params|target|warnings|[^]{joined|separate|undocumented}}[,...]
                           Display specific types of command line options
  (Use '-v --help' to display command line options of sub-processes)
  --version                Display compiler version information
  -dumpspecs               Display all of the built in spec strings
  -dumpversion             Display the version of the compiler
  -dumpmachine             Display the compiler's target processor
  -print-search-dirs       Display the directories in the compiler's search path
  -print-libgcc-file-name  Display the name of the compiler's companion library
  -print-file-name=<lib>   Display the full path to library <lib>
  -print-prog-name=<prog>  Display the full path to compiler component <prog>
  -print-multiarch         Display the target's normalized GNU triplet, used as
                           a component in the library path
  -print-multi-directory   Display the root directory for versions of libgcc
  -print-multi-lib         Display the mapping between command line options and
                           multiple library search directories
  -print-multi-os-directory Display the relative path to OS libraries
  -print-sysroot           Display the target libraries directory
  -print-sysroot-headers-suffix Display the sysroot suffix used to find headers
  -Wa,<options>            Pass comma-separated <options> on to the assembler
  -Wp,<options>            Pass comma-separated <options> on to the preprocessor
  -Wl,<options>            Pass comma-separated <options> on to the linker
  -Xassembler <arg>        Pass <arg> on to the assembler
  -Xpreprocessor <arg>     Pass <arg> on to the preprocessor
  -Xlinker <arg>           Pass <arg> on to the linker
  -save-temps              Do not delete intermediate files
  -save-temps=<arg>        Do not delete intermediate files
  -no-canonical-prefixes   Do not canonicalize paths when building relative
                           prefixes to other gcc components
  -pipe                    Use pipes rather than intermediate files
  -time                    Time the execution of each subprocess
  -specs=<file>            Override built-in specs with the contents of <file>
  -std=<standard>          Assume that the input sources are for <standard>
  --sysroot=<directory>    Use <directory> as the root directory for headers
                           and libraries
  -B <directory>           Add <directory> to the compiler's search paths
  -v                       Display the programs invoked by the compiler
  -###                     Like -v but options quoted and commands not executed
  -E                       Preprocess only; do not compile, assemble or link
  -S                       Compile only; do not assemble or link
  -c                       Compile and assemble, but do not link
  -o <file>                Place the output into <file>
  -pie                     Create a position independent executable
  -shared                  Create a shared library
  -x <language>            Specify the language of the following input files
                           Permissible languages include: c c++ assembler none
                           'none' means revert to the default behavior of
                           guessing the language based on the file's extension

Options starting with -g, -f, -m, -O, -W, or --param are automatically
 passed on to the various sub-processes invoked by gcc.  In order to pass
 other options on to these processes the -W<letter> options must be used.

For bug reporting instructions, please see:
<http://bugzilla.redhat.com/bugzilla>.
[root@jacklin operating-system]# 

```

### Vim 命令补充

:q!  不保存强制退出