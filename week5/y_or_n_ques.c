#include <stdio.h>

int y_or_n_ques(const char * question)
{
	fputs(question, stdout);
	while(1)
	{
		int c, answer;
		
		fputc(" ", stdout);
		
		// tolower 作用：将字母转换成小写字母	
		c = tolower(fgetc(stdin));
		answer = c;
		
		while(c != '\n' && c != EOF)
			c = fgetc(stdin);

		if(answer == 'y')
			return 1;
		if(answer == 'n')
			return 0;

		fputs("Please answer y or n:", stdout);
	}
}
