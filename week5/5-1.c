#include "ch02.h"
int main()
{
	// 使用 setbuf 函数关闭缓冲，第二个参数需要设置为 NULL
	// 标准输入输出对应的流分别为：stdin，stdout
	// 标准错误流是：stderr
	

	// 第1中方式，关闭缓冲来实现输出;
	//setbuf(stdout, NULL);
	
	// 另一种方式：使用for循环，填满该缓冲区

	// 第2种方式，指定缓冲区大小
	//char buf[2];
	//setvbuf(stdout, buf, _IOLBF, 3);
	
	// printf 默认是行缓冲
	printf("hello!");
	int i;
	for(i = 0; i < 1024; i++)
	{
		printf(" ");
	}

	// 第3种方式，了利用 fflush 或者 fclose 函数
	//fflush(stdout);
	while(1);
	return 0;
}
