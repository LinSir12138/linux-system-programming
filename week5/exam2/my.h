#include <stdio.h>
#include <string.h>

#ifndef _MY_H
#define _MY_H

struct user{
	int id;
	char name[8];
	char htel[12];
	char tel[12];
};

void input(struct user * puser);
void save(struct user * puser);
void mysort();
void output();

#endif
