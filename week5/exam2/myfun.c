#include "my.h"
#include <stdlib.h>
#include <string.h>

int char_cmp1(const void* x, const void* y)
{
	// 升序排列
	//因为数组里存的是字符串的地址，所以要强制类型转换成(char **)
	//然后再解引用一下才是字符串的地址
	return strcmp(*(char**)x, *(char**)y);
	
}

int char_cmp2(const void* x, const void* y)
{
	// 降序排列
	//因为数组里存的是字符串的地址，所以要强制类型转换成(char **)
	//然后再解引用一下才是字符串的地址
	return strcmp(*(char**)y, *(char**)x);
	
}

void input(struct user * puser)
{
	// 定义一个结构体变量,这里改用结构体指针
	//struct user user1;
	
	printf("请输入用户的id:\n");
	// 注意：要有取地址符号
	scanf("%d", &(puser->id));
	//fflush(stdin);

	printf("请输入用户的name\n");
	//gets(user1.name);
	scanf("%s", &(puser->name));
	
	printf("请输入用户的htel\n");
	//gets(user1.htel);
	scanf("%s", &(puser->htel));

	printf("请输入用户的tel\n");
	//gets(user1.tel);
	scanf("%s", &(puser->tel));

	// 下面这部分拆分到 save 函数里面去
	//FILE * stream;
	//stream = fopen("./user.dat", "a");

	// 使用 fputs() 写入字符串，这里要写入数字，所以使用 fprintf()
	//fprintf(stream, "%d", puser->id);
	//fputs(idstr, stream);
	//fputs(" ", stream);
	

	//fputs(puser->name, stream);
	//fputs(" ", stream);

	//fputs(puser->htel, stream);
	//fputs(" ", stream);

	//fputs(puser->tel, stream);
	//fputs("\n", stream);

	//fclose(stream);
}

void save(struct user * puser)
{
	
	FILE * stream;

	stream = fopen("./user.dat", "a");

	// 使用 fputs() 写入字符串，这里要写入数字，所以使用 fprintf()
	fprintf(stream, "%d", puser->id);
	//fputs(idstr, stream);
	fputs(" ", stream);

	fputs(puser->name, stream);
	fputs(" ", stream);

	fputs(puser->htel, stream);
	fputs(" ", stream);

	fputs(puser->tel, stream);
	fputs("\n", stream);

	fclose(stream);
}

void mysort()
{
	// 用来统计共有多少条记录
	int number = 0;

	FILE *stream = fopen("./user.dat", "r");
	char buf[50];

	// 直接一个 while 循环，统计有多少条记录
	while(fgets(buf, 50, stream))
	{
		printf("buf1=%s\n", buf);
		number++;
	}
	printf("共有%d条记录\n", number);

	// 重置文件位置
	fseek(stream, 0, SEEK_SET);
	
	// 创建一个字符串数组，用来存放所以数据，一个元素就是一条记录
	char * records[number];

	// 初始化该字符串数组
	int flag1;
	for(flag1 = 0; flag1 < number; flag1++) 
	{
		records[flag1] = (char*)malloc(50*sizeof(char));
	}

	// 为该字符数组赋值
	int i = 0;
	while(fgets(buf, 50, stream))
	{
		printf("buf=%s\n", buf);
		strcpy(records[i], buf);
		i++;
	}
	// 重置文件位置
	fseek(stream, 0, SEEK_SET);

	// 赋值之后，循环打印一下改字符串数组
	int j;
	for(j = 0; j < number; j++)
	{
		printf("数组元素为%s\n", records[j]);
	}


	// 定义一个字符串数组，用来保存名字,有多少条记录，就有多少个名字
	char * names[number];
	// 为该字符串数组初始化赋值
	int flag2;
	for(flag2 = 0; flag2 < number; flag2++)
	{
		names[flag2] = (char*)malloc(50*sizeof(char));
	}

	// 循环遍历 records 数组，从中获取 name，并存入 names 字符串数组中
	int flag3;
	for(flag3 = 0; flag3 < number; flag3++)
	{
		// 从一条记录字符串中获取name属性
		// 分别表示1行字符串中前2个空格的位置
		int space1 = -1;
		int space2 = -1;
		// 利用for循环找出前2个空格的位置
		int i;
		for(i = 0; i < 50; i++)
		{
			if(records[flag3][i] == ' ')
			{
				if(space1 == -1)
					space1 = i;	
				else if(space2 == -1)
					space2 = i;
				else
					break;
			}
		}
		printf("f1=%d f2=%d\n", space1, space2);	
		// 重置两个空格的位置

		// 为一个 names 中的元素赋值
		// j 用来表示 records 中一个字符串元素的下标
		int j;
		// k 用来表示 names 中一个字符串元素的下标
		int k = 0;
		for(j = space1 + 1; j < space2; j++)
		{
			// 此时 flag3 表示当前是 names 的第几个元素
			names[flag3][k] = records[flag3][j];
			k++;
		}
		printf("names[%d] = %s\n", flag3, names[flag3]);

		// 最后还要重置一下两个空格的位置
		space1 = -1;
		space2 = -1;
	}

	//到这里，文件里的名字已经存放在字符串数组 names 中了，直接调用排序函数进行排序即可

	printf("请选择排序方式：1 升序，2 降序");
	int sortType;
	scanf("%d", &sortType);


	if(sortType == 1) 
	{
		qsort(names, sizeof(names) / sizeof(names[0]), sizeof(char *), char_cmp1);
	}
	else if(sortType == 2)
	{
		qsort(names, sizeof(names) / sizeof(names[0]), sizeof(char *), char_cmp2);
	}
	else 
	{
		printf("输入不合法，默认按照升序排序");
		qsort(names, sizeof(names) / sizeof(names[0]), sizeof(char *), char_cmp1);
	}

	
	
	// 遍历输出排序之后的
	printf("\n-------排序之后的名字如下所示-------\n");
	int flag4;
	for(flag4 = 0; flag4 < number; flag4++)
	{
		printf("name=%s\n", names[flag4]);
	}
}

void output()
{
	printf("test output!\n");
	FILE * stream;
	stream = fopen("./user.dat", "r");
	char buf[1024];
	fread(buf, 1024, 1, stream);

	printf("userid name htel tel\n");
	printf("%s", buf);
	fclose(stream);
}
