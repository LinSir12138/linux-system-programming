#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int char_cmp1(const void* x, const void* y)
{
	// 升序排列
	//因为数组里存的是字符串的地址，所以要强制类型转换成(char **)
	//然后再解引用一下才是字符串的地址
	return strcmp(*(char**)x, *(char**)y);
	
}

int char_cmp2(const void* x, const void* y)
{
	// 降序排列
	//因为数组里存的是字符串的地址，所以要强制类型转换成(char **)
	//然后再解引用一下才是字符串的地址
	return strcmp(*(char**)y, *(char**)x);
	
}

int main()
{
	char *str[] = {"h", "a", "d", "c", "f", "e", "b", "g", "1", "2", "5", "3", "4"};
	qsort(str, sizeof(str) / sizeof(str[0]), sizeof(char *), char_cmp2);	
	int i;
	for(i = 0; i < sizeof(str) / sizeof(str[0]); i++)
	{
		printf("%s\n", str[i]);
	}
	return 0;
}
